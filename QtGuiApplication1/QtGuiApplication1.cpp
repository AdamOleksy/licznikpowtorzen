#include "QtGuiApplication1.h"
#include <QCoreApplication>
#include "qstring.h"
#include <string>
#include <QDebug>
#include "Konwerter.h"

QtGuiApplication1::QtGuiApplication1(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	//on_pushButton1_clicked();
	connect(ui.pushButtonCofnij, SIGNAL(click()),
		ui.label, SLOT(on_pushButtonCofnij_clicked()));

	connect(ui.pushButton1, SIGNAL(click()),
		ui.label, SLOT(on_pushButton1_clicked()));

	connect(ui.pushButton2, SIGNAL(click()),
		ui.label, SLOT(on_pushButton2_clicked()));

	connect(ui.pushButton3, SIGNAL(click()),
		ui.label, SLOT(on_pushButton3_clicked()));

	connect(ui.pushButton5, SIGNAL(click()),
		ui.label, SLOT(on_pushButton5_clicked()));

	connect(ui.pushButton10, SIGNAL(click()),
		ui.label, SLOT(on_pushButton10_clicked()));

	connect(ui.pushButton15, SIGNAL(click()),
		ui.label, SLOT(on_pushButton15_clicked()));

	connect(ui.pushButton20, SIGNAL(click()),
		ui.label, SLOT(on_pushButton20_clicked()));

	connect(ui.pushButtonZapiszCel, SIGNAL(click()),
		ui.label, SLOT(on_pushButtonCofnij_clicked()));

	zegar = new QTimer(this);
	connect(zegar, SIGNAL(timeout()), this, SLOT(funkcja_zegara()));

	
	
}

void QtGuiApplication1::wyswietl_liczbaPowt()
{
	QString cos = std::to_string(liczba_powt).c_str();
	int postep = (liczba_powt* 100)/cel;
	ui.progressBar->setValue(postep);
	ui.label->setText(cos);
	int interwal = (licznik_czasu - czas_rozpoczecia_interwalu);
	if (interwal > 2) 
	{
		ui.label_czas_interwlu->setText("Czas interwalu: " + konwerter.ZamienCzasNaString(interwal));
	}
	zeruj_interwal();
	if (liczba_powt >= cel)
	{
		zegar->stop();
		ui.label->setText("Zrobiles to");
	}
}

void QtGuiApplication1::funkcja_zegara()
{
	licznik_czasu++;
	ui.label_czas_od_rozpoczecia->setText("Czas cwiczenia: " + konwerter.ZamienCzasNaString(licznik_czasu));
}

void QtGuiApplication1::on_pushButtonCofnij_clicked()
{

	if(pamiec_cofnij.size() >= 1)
	{
		int i = pamiec_cofnij.back();
		i *= -1;
		zwieksz_wartosc_liczbaPowt(i);

		pamiec_cofnij.pop_back();
	}
	
}

void QtGuiApplication1::on_pushButton1_clicked()
{
	zwieksz_wartosc_liczbaPowt(1);
}

void QtGuiApplication1::zeruj_interwal()
{
	czas_rozpoczecia_interwalu = licznik_czasu;
}

void QtGuiApplication1::on_pushButton2_clicked()
{
	zwieksz_wartosc_liczbaPowt(2);
}

void QtGuiApplication1::on_pushButton3_clicked()
{
	zwieksz_wartosc_liczbaPowt(3);
}

void QtGuiApplication1::on_pushButton5_clicked()
{
	zwieksz_wartosc_liczbaPowt(5);
}

void QtGuiApplication1::on_pushButton10_clicked()
{
	zwieksz_wartosc_liczbaPowt(10);
}

void QtGuiApplication1::on_pushButton15_clicked()
{
	zwieksz_wartosc_liczbaPowt(15);
}

void QtGuiApplication1::on_pushButton20_clicked()
{
	zwieksz_wartosc_liczbaPowt(20);
	
}

void QtGuiApplication1::on_pushButtonZapiszCel_clicked()
{
	try
	{
		ustaw_cel(std::stoi(ui.textEdit->toPlainText().toStdString()));
	}
	catch (std::invalid_argument)
	{
		ui.label->setText("Niewlasciwa liczba");
		return;
	}

	ui.pushButtonZapiszCel->setEnabled(0);
	zegar->start(1000);
}

void QtGuiApplication1::wyswietl_pobranie_celu()
{
	QString cos = "Pobrano cel: ";
	cos += std::to_string(cel).c_str();
	ui.label->setText(cos);
}
