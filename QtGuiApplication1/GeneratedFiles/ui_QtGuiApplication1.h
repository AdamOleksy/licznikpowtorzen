/********************************************************************************
** Form generated from reading UI file 'QtGuiApplication1.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QTGUIAPPLICATION1_H
#define UI_QTGUIAPPLICATION1_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QtGuiApplication1Class
{
public:
    QWidget *centralWidget;
    QPushButton *pushButton1;
    QPushButton *pushButton2;
    QPushButton *pushButton3;
    QPushButton *pushButton5;
    QPushButton *pushButton10;
    QPushButton *pushButton15;
    QPushButton *pushButton20;
    QLabel *label;
    QTextEdit *textEdit;
    QLabel *label_2;
    QPushButton *pushButtonZapiszCel;
    QProgressBar *progressBar;
    QLabel *label_czas_od_rozpoczecia;
    QLabel *label_czas_interwlu;
    QPushButton *pushButtonCofnij;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *QtGuiApplication1Class)
    {
        if (QtGuiApplication1Class->objectName().isEmpty())
            QtGuiApplication1Class->setObjectName(QStringLiteral("QtGuiApplication1Class"));
        QtGuiApplication1Class->resize(567, 523);
        centralWidget = new QWidget(QtGuiApplication1Class);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        pushButton1 = new QPushButton(centralWidget);
        pushButton1->setObjectName(QStringLiteral("pushButton1"));
        pushButton1->setGeometry(QRect(120, 160, 35, 35));
        pushButton2 = new QPushButton(centralWidget);
        pushButton2->setObjectName(QStringLiteral("pushButton2"));
        pushButton2->setGeometry(QRect(170, 160, 35, 35));
        pushButton3 = new QPushButton(centralWidget);
        pushButton3->setObjectName(QStringLiteral("pushButton3"));
        pushButton3->setGeometry(QRect(220, 160, 35, 35));
        pushButton5 = new QPushButton(centralWidget);
        pushButton5->setObjectName(QStringLiteral("pushButton5"));
        pushButton5->setGeometry(QRect(270, 160, 35, 35));
        pushButton10 = new QPushButton(centralWidget);
        pushButton10->setObjectName(QStringLiteral("pushButton10"));
        pushButton10->setGeometry(QRect(320, 160, 35, 35));
        pushButton15 = new QPushButton(centralWidget);
        pushButton15->setObjectName(QStringLiteral("pushButton15"));
        pushButton15->setGeometry(QRect(370, 160, 35, 35));
        pushButton20 = new QPushButton(centralWidget);
        pushButton20->setObjectName(QStringLiteral("pushButton20"));
        pushButton20->setGeometry(QRect(420, 160, 35, 35));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(120, 120, 331, 31));
        textEdit = new QTextEdit(centralWidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(130, 40, 181, 31));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(130, 20, 111, 16));
        pushButtonZapiszCel = new QPushButton(centralWidget);
        pushButtonZapiszCel->setObjectName(QStringLiteral("pushButtonZapiszCel"));
        pushButtonZapiszCel->setGeometry(QRect(320, 50, 75, 23));
        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setGeometry(QRect(120, 200, 381, 21));
        progressBar->setValue(0);
        label_czas_od_rozpoczecia = new QLabel(centralWidget);
        label_czas_od_rozpoczecia->setObjectName(QStringLiteral("label_czas_od_rozpoczecia"));
        label_czas_od_rozpoczecia->setGeometry(QRect(120, 100, 171, 16));
        label_czas_interwlu = new QLabel(centralWidget);
        label_czas_interwlu->setObjectName(QStringLiteral("label_czas_interwlu"));
        label_czas_interwlu->setGeometry(QRect(120, 240, 181, 16));
        pushButtonCofnij = new QPushButton(centralWidget);
        pushButtonCofnij->setObjectName(QStringLiteral("pushButtonCofnij"));
        pushButtonCofnij->setGeometry(QRect(470, 160, 41, 35));
        QtGuiApplication1Class->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(QtGuiApplication1Class);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 567, 21));
        QtGuiApplication1Class->setMenuBar(menuBar);
        mainToolBar = new QToolBar(QtGuiApplication1Class);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        QtGuiApplication1Class->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(QtGuiApplication1Class);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        QtGuiApplication1Class->setStatusBar(statusBar);

        retranslateUi(QtGuiApplication1Class);

        QMetaObject::connectSlotsByName(QtGuiApplication1Class);
    } // setupUi

    void retranslateUi(QMainWindow *QtGuiApplication1Class)
    {
        QtGuiApplication1Class->setWindowTitle(QApplication::translate("QtGuiApplication1Class", "QtGuiApplication1", nullptr));
        pushButton1->setText(QApplication::translate("QtGuiApplication1Class", "+1", nullptr));
        pushButton2->setText(QApplication::translate("QtGuiApplication1Class", "+2", nullptr));
        pushButton3->setText(QApplication::translate("QtGuiApplication1Class", "+3", nullptr));
        pushButton5->setText(QApplication::translate("QtGuiApplication1Class", "+5", nullptr));
        pushButton10->setText(QApplication::translate("QtGuiApplication1Class", "+10", nullptr));
        pushButton15->setText(QApplication::translate("QtGuiApplication1Class", "+15", nullptr));
        pushButton20->setText(QApplication::translate("QtGuiApplication1Class", "+20", nullptr));
        label->setText(QApplication::translate("QtGuiApplication1Class", "Suma:", nullptr));
        label_2->setText(QApplication::translate("QtGuiApplication1Class", "Cel na dzisiaj", nullptr));
        pushButtonZapiszCel->setText(QApplication::translate("QtGuiApplication1Class", "Zapisz cel", nullptr));
        label_czas_od_rozpoczecia->setText(QApplication::translate("QtGuiApplication1Class", "Czas cwiczenia:", nullptr));
        label_czas_interwlu->setText(QApplication::translate("QtGuiApplication1Class", "Czas interwalu:", nullptr));
        pushButtonCofnij->setText(QApplication::translate("QtGuiApplication1Class", "cofnij", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QtGuiApplication1Class: public Ui_QtGuiApplication1Class {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QTGUIAPPLICATION1_H
