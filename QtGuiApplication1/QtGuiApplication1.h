#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_QtGuiApplication1.h"
#include <QTimer>
#include <vector>
#include "Konwerter.h"

class QtGuiApplication1 : public QMainWindow
{
	Q_OBJECT

public:
	QtGuiApplication1(QWidget *parent = Q_NULLPTR);
	void zeruj_interwal();

private:
	Ui::QtGuiApplication1Class ui;
	int liczba_powt = 0;
	int cel =1;
	QTimer *zegar;
	int licznik_czasu = 0;
	int czas_rozpoczecia_interwalu = 0;
	std::vector<int> pamiec_cofnij;
	Konwerter konwerter;

private slots:
	void on_pushButton1_clicked();
	void on_pushButton2_clicked();
	void on_pushButton3_clicked();
	void on_pushButton5_clicked();
	void on_pushButton10_clicked();
	void on_pushButton15_clicked();
	void on_pushButtonZapiszCel_clicked();
	void on_pushButton20_clicked();
	void on_pushButtonCofnij_clicked();
	void funkcja_zegara();

public:
	void wyswietl_pobranie_celu();
	void ustaw_cel(int i)
	{
		cel = i;
		wyswietl_pobranie_celu();
	}
	void zwieksz_wartosc_liczbaPowt(int i)
	{
		liczba_powt += i;
		if (i > 0)
		{
			pamiec_cofnij.push_back(i);
			if (pamiec_cofnij.size() > 5)
				pamiec_cofnij.erase(pamiec_cofnij.begin());
		}
		
		wyswietl_liczbaPowt();
	}
	void wyswietl_liczbaPowt();


	

};
