#include "Konwerter.h"



QString Konwerter::ZamienCzasNaString(int sek)
{
		int sekunda = (sek) % 60;
		int minuty = (sek) / 60;
		std::string wyswietlanie_czasu;
		wyswietlanie_czasu += " ";
		if (minuty < 10)
			wyswietlanie_czasu += "0";
		wyswietlanie_czasu += std::to_string(minuty);
		wyswietlanie_czasu += ":";
		if (sekunda < 10)
			wyswietlanie_czasu += "0";
		wyswietlanie_czasu += std::to_string(sekunda);
		QString czas = (wyswietlanie_czasu).c_str();
		return czas;
}

Konwerter::Konwerter()
{
}


Konwerter::~Konwerter()
{
}
